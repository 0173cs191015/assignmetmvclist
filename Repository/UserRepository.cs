﻿using CRUD_OperationAssignment.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace CRUD_OperationAssignment.Repository
{
    public class UserRepository : IUserRepository
    {
        List<User> _users;
        public UserRepository()
        {
            _users = new List<User>();
        }

        public List<User> GetAllUser()
        {
            return _users;
        }
    }
}
