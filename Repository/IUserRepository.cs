﻿using CRUD_OperationAssignment.Models;
using System.Collections.Generic;

namespace CRUD_OperationAssignment.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUser();
    }
}
