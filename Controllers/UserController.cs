﻿using CRUD_OperationAssignment.Models;
using CRUD_OperationAssignment.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CRUD_OperationAssignment.Controllers
{
    public class UserController : Controller
    {
        readonly UserService _userService;
        public UserController(UserService userService)
        {
            _userService = userService;
        }

        //Action-Index-List<user> Get All Users
        public IActionResult Index()
        {
            List<User> UserList= _userService.GetAllUser();
            return View(UserList);
        }
    }
}
