# UserApp

# Features
* CRUD
* Create---------C
* Read(GET)------R
* Update(Edit)---U
* Delete---------D

# Tech Stacks
* C#
* ASP.Net core MVC
* LINQ


# Code Standard
* Layered Approach
  Controller ----->Service ----> Repository ---->

* Seperation of Concern

