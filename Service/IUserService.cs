﻿using CRUD_OperationAssignment.Models;
using System.Collections.Generic;

namespace CRUD_OperationAssignment.Service
{
    public interface IUserService
    {
        List<User> GetAllUser();
    }
}
