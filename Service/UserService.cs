﻿using CRUD_OperationAssignment.Models;
using CRUD_OperationAssignment.Repository;
using System;
using System.Collections.Generic;

namespace CRUD_OperationAssignment.Service
{
    public class UserService : IUserService
    {
        readonly UserRepository _userRepository;
        public UserService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<User> GetAllUser()
        {
            List<User> usersList = _userRepository.GetAllUser();
            return usersList;
        }
    }
}
